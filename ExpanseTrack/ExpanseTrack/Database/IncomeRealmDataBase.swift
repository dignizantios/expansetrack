//
//  RealmDataBase.swift
//  ExpanseTrack
//
//  Created by Haresh on 06/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import RealmSwift

class IncomeRealmDataBase: Object {
    
    @objc dynamic var id            = 0
    @objc dynamic var incomeData    = ""
    @objc dynamic var amount        = ""
    @objc dynamic var monthIndex    = Int()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
   
}

