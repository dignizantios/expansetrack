//
//  ExpanceRealmDataBase.swift
//  ExpanseTrack
//
//  Created by Haresh on 08/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ExpanceRealmDataBase: Object {
    
    @objc dynamic var id            = 0
    @objc dynamic var expanceName   = ""
    @objc dynamic var amount        = ""
    @objc dynamic var monthIndex    = Int()
        
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
