//
//  CustomLabel.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit



@IBDesignable
public class CustomLabel : UILabel {
    
    /// Applies border to the Button with the specified width
    @IBInspectable public var borderWidth: CGFloat = 1.0 {
        didSet{
            layer.borderWidth = borderWidth
            layer.borderColor = borderColor.cgColor
        }
    }
    
    /// Sets the color of the border
    @IBInspectable public var borderColor: UIColor = .clear {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    /// Make the corners rounded with the specified radius
    @IBInspectable public var cornerRadious: CGFloat = 20.0 {
        didSet{
            layer.cornerRadius = cornerRadious
        }
    }
}

