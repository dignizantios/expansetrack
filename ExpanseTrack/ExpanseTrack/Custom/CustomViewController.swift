//
//  CustomViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    //MARK: Navigation Left Button
    func navigationLeftButton() -> UIButton {
        
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //        backButton.frame = CTFrame()
        backButton.setImage(UIImage(named: "ic_back_arrow"), for: .normal) // Image can be downloaded from here below link
        backButton.setTitle("", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        
        return backButton
    }
    
    @objc func backAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Alert
    func displayAlertMessage(userMessage: String) {
        
        let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension Collection where Element: StringProtocol, Element.Index == String.Index {
    public func localizedStandardSorted(_ result: ComparisonResult) -> [Element] {
        return sorted { $0.localizedStandardCompare($1) == result }
    }
}
