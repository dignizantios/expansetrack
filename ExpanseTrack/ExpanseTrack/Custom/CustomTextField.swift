//
//  CustomTextField.swift
//  ExpanseTrack
//
//  Created by Haresh on 06/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class CustomTextField:UITextField {
    
    /// Applies border to the text view with the specified width
    @IBInspectable public var borderWidth: CGFloat = 1.0 {
        didSet{
            layer.borderWidth = borderWidth
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
    /// Sets the color of the border
    @IBInspectable public var borderColor: UIColor = .clear {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    /// Make the corners rounded with the specified radius
    @IBInspectable public var cornerRadious: CGFloat = 20.0 {
        didSet{
            layer.cornerRadius = cornerRadious
        }
    }
    
    /// Sets the placeholder color
    @IBInspectable public var placeholderColor: UIColor = .lightGray {
        didSet{
            let placeholderString = placeholder ?? ""
            attributedPlaceholder = NSAttributedString(string: placeholderString, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
            
        }
    }
    
    /// Sets the Left Image
    @IBInspectable var leftImage : UIImage? {
        didSet {
            if let image = leftImage{
                leftViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 15, y: 0, width: 16, height: 20))
                imageView.image = image
                imageView.contentMode = .center
                imageView.tintColor = tintColor
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 40, height: 20))
                view.addSubview(imageView)
                leftView = view
            }else {
                leftViewMode = .never
            }
            
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setupRightImage(strint: String) {
        
        // set frame on image before adding it to the uitextfield
        let view = UIView()
        view.frame = CGRect(x: self.frame.width - 15, y: 5, width: 25, height: 20)
        
        let imgSearch = UIImageView()
        imgSearch.image = UIImage(named: strint)
        imgSearch.frame = CGRect(x: 0 , y: 3, width: 13, height: 13)
        
        view.addSubview(imgSearch)
        
        self.rightView = view
        self.rightViewMode = .always
    }
}
