//
//  ExpanseTableViewCell.swift
//  ExpanseTrack
//
//  Created by Haresh on 08/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

class ExpanseTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDataName: UILabel!
    @IBOutlet weak var lblDataAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
