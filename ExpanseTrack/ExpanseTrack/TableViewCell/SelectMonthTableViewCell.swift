//
//  SelectMonthTableViewCell.swift
//  ExpanseTrack
//
//  Created by Haresh on 09/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

class SelectMonthTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblSelectMonth: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
