//
//  DatabaseTableViewCell.swift
//  ExpanseTrack
//
//  Created by Haresh on 06/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

class DatabaseTableViewCell: UITableViewCell {

    
//    @IBOutlet weak var incomeStackView      : CustomStackView!
    
    @IBOutlet weak var lblSection: UILabel!
    
    @IBOutlet weak var viewIncome: UIView!
    @IBOutlet weak var IncomelabelAmount    : UILabel!
    
    @IBOutlet weak var viewExpanse: UIView!
    @IBOutlet weak var ExpanselabelAmount   : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
