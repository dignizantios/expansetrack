//
//  IncomeViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit
import CarbonKit
import RealmSwift
import SwiftyJSON

class IncomeViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var textFieldName    : UITextField!
    @IBOutlet weak var textFieldAmount  : UITextField!
    @IBOutlet weak var buttonAdd        : UIButton!
    
    @IBOutlet weak var tblIncomeData    : UITableView!
    @IBOutlet weak var viewIncome       : UIView!
    @IBOutlet weak var btnDone          : UIButton!
    
    var selectMonthIndex = Int()
    
    var incomeDataResult : Results<IncomeRealmDataBase>?
    var monthValueArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Month Index:", selectMonthIndex)
        incomeDataResult = realm.objects(IncomeRealmDataBase.self)
        
        self.viewIncome.isHidden = true
        self.tblIncomeData.register(UINib(nibName: "IncomeTableViewCell", bundle: nil), forCellReuseIdentifier: "IncomeTableViewCell")
        monthValueArray.removeAllObjects()
        self.tblIncomeData.tableFooterView = UIView()
        print("Income Realm File path url:", Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        [textFieldAmount, textFieldName].forEach { (textField) in
            textField?.setLeftPaddingPoints(10)
        }
        self.tblIncomeData.isHidden = false
        self.viewIncome.isHidden = true        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldAmount {
            let allowedCharacters = "1234567890"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        return true
    }
    
    @IBAction func buttonAddAction(_ sender: Any) {
        self.tblIncomeData.isHidden = true
        self.viewIncome.isHidden = false
    }
    
    func addData() {
        
        let incomeData = IncomeRealmDataBase()
        incomeData.id = getincrementID()
        incomeData.monthIndex = self.selectMonthIndex
        incomeData.incomeData = textFieldName.text!
        incomeData.amount = textFieldAmount.text!
        
        try! realm.write {
            realm.add(incomeData)
        }
    }
    
    //Primary key increment
    func getincrementID() -> Int {
        return (realm.objects(IncomeRealmDataBase.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        
        
        if (self.textFieldName.text?.isEmpty == true) || (self.textFieldAmount.text?.isEmpty == true) {
            self.displayAlertMessage(userMessage: "Please Fill all textField")
        }
        else {
            textFieldAmount.resignFirstResponder()
            self.addData()
            
            textFieldAmount.text = ""
            textFieldName.text = ""
            self.viewIncome.isHidden = true
            self.tblIncomeData.isHidden = false
            self.displayAlertMessage(userMessage: "New Data add")
        }
        self.tblIncomeData.reloadData()
        self.tblIncomeData.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.monthValueArray.removeAllObjects()
        for item in realm.objects(IncomeRealmDataBase.self).filter("monthIndex == \(self.selectMonthIndex)") {
            self.monthValueArray.add(item)
        }
        return self.monthValueArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeTableViewCell", for: indexPath) as! IncomeTableViewCell
        
        let incomeDic = self.monthValueArray[indexPath.row]
        
        cell.lblDataName.text = (incomeDic as AnyObject).incomeData
        cell.lblDataAmount.text = (incomeDic as AnyObject).amount
        
        return cell
    }
    
}
