//
//  ViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit
import DropDown
import RealmSwift

class ViewController: UIViewController {

    
    @IBOutlet weak var btnSummary       : UIBarButtonItem!
    @IBOutlet weak var labelSelectMonth : CustomLabel!
    @IBOutlet weak var buttonSelectMonth: CustomButton!
    
    @IBOutlet weak var tblSelectedMonth: UITableView!
    
    var arrayMonth  = NSArray()
    let dropDown    = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dropDowndata()
        self.tblSelectedMonth.tableFooterView = UIView()
        self.arrayMonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    }

    override func viewWillAppear(_ animated: Bool) {
        
        
        
        self.tblSelectedMonth.layer.cornerRadius = 10
        self.tblSelectedMonth.layer.borderColor = UIColor(displayP3Red: 38/255, green: 28/255, blue: 171/255, alpha: 1.0).cgColor
        self.tblSelectedMonth.layer.borderWidth = 1.0
    }
    
    func dropDowndata() {
        
        dropDown.dataSource = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    }
    
    @IBAction func btnSummaryAction(_ sender: Any) {
        let databaseVC = self.storyboard?.instantiateViewController(withIdentifier: "DatabaseViewController") as! DatabaseViewController
        self.navigationController?.pushViewController(databaseVC, animated: true)
    }
    
    
    @IBAction func buttonSelectMonthAction(_ sender: Any) {
//        let incomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TopBarViewController") as! TopBarViewController
//        let IncomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopBarViewController") as! IncomeViewController
//        let expanseDataVC = self.storyboard?.instantiateViewController(withIdentifier: "ExpenseViewController") as! ExpenseViewController
//
//        dropDown.direction = .bottom
//
//        dropDown.bottomOffset = CGPoint(x: labelSelectMonth.frame.origin.x, y: labelSelectMonth.frame.origin.y + labelSelectMonth.frame.height)
//        dropDown.width = labelSelectMonth.frame.width
//
//        print("labelSelectMonth.frame.origin.x:", labelSelectMonth.frame.origin.x)
//        print("labelSelectMonth.frame.origin.y:", labelSelectMonth.frame.origin.y + labelSelectMonth.frame.height)
//
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            self.labelSelectMonth.text = item
//
//            incomeVC.monthIndex = index
////            incomeDataVC.monthIndex = index
////            expanseDataVC.monthIndex = index
//
////            if (self.labelSelectMonth.text == item) {
//            incomeVC.navigationTitle = item
//            self.navigationController?.pushViewController(incomeVC, animated: true)
////            }
//
//        }
//        dropDown.show()
//
//        self.navigationController?.pushViewController(incomeVC, animated: true)
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMonth.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMonthTableViewCell", for: indexPath) as! SelectMonthTableViewCell
        
        cell.lblSelectMonth.text = arrayMonth[indexPath.row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let incomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TopBarViewController") as! TopBarViewController
        
        incomeVC.navigationTitle = self.arrayMonth[indexPath.row] as! String
        incomeVC.monthIndex = indexPath.row
        print("indexPath.row:\(indexPath.row)")
        
        self.navigationController?.pushViewController(incomeVC, animated: true)
    }
}

