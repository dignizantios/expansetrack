//
//  DatabaseViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 06/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class DatabaseViewController: UIViewController {

    //Outlets
    @IBOutlet weak var btnShare         : UIBarButtonItem!
    @IBOutlet weak var tblDataBase      : UITableView!
    @IBOutlet weak var lblTotalIncome   : UILabel!
    @IBOutlet weak var lblTotalExpanse  : UILabel!
    @IBOutlet weak var lblTotalProfit   : UILabel!
    
    @IBOutlet weak var viewDataBaseBottom: UIView!
    
    //Variable Declarartion
    var monthArray : [String] = []
    
    var incomeValue  = Int()
    var expanseValue = Int()
    
    var totalIncom   = Int()
    var totalExpanse = Int()
    
    var arrData : [JSON]  = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        
        tblDataBase.register(UINib(nibName: "DatabaseTableViewCell", bundle: nil), forCellReuseIdentifier: "DatabaseTableViewCell")
        tblDataBase.tableFooterView = UIView()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.tblDataBase.reloadData()
        
        self.incomeValue  = 0
        self.expanseValue = 0
        
        totalExpanse = 0
        totalIncom   = 0
        
        self.tblDataBase.rowHeight = 110
                
        /*for i in 0..<12{
            incomeFilter(i)
        }
        for i in 0..<12 {
            expanseFilter(i)
        }*/
        
        for i in 0..<monthArray.count
        {
            GetIncomeExpense(i)
        }
        
        self.tblDataBase.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewDataBaseBottom.layer.borderWidth    = 1
        viewDataBaseBottom.layer.borderColor    = UIColor(displayP3Red: 38/255, green: 14/255, blue: 171/255, alpha: 1.0).cgColor
        viewDataBaseBottom.layer.cornerRadius   = 10
        
        self.tblDataBase.layer.borderColor  = UIColor(displayP3Red: 38/255, green: 14/255, blue: 171/255, alpha: 1.0).cgColor
        tblDataBase.layer.cornerRadius      = 10
        self.tblDataBase.layer.borderWidth  = 1.0
        self.title = "Database"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navigationLeftButton())
    }
    
    //Get Database
    func GetIncomeExpense(_ index:Int)
    {
        //-----------Income data-----------//
        incomeValue = 0
        var dict = JSON()
        for item in realm.objects(IncomeRealmDataBase.self).filter("monthIndex == \(index)") {
            self.incomeValue = self.incomeValue + Int(item.amount)!
        }
        dict["index"].stringValue = String(index)
        dict["income"].stringValue = String(self.incomeValue)
        
        self.totalIncom = totalIncom + self.incomeValue
        print("self.totalIncom:- \(self.totalIncom)")
        
        //-----------Expense data---------//
        expanseValue = 0
        for item in realm.objects(ExpanceRealmDataBase.self).filter("monthIndex == \(index)") {
            self.expanseValue = self.expanseValue + Int(item.amount)!
        }
        
        self.totalExpanse = totalExpanse + self.expanseValue
        print("self.totalExpanse:- \(self.totalExpanse)")
        dict["expense"].stringValue = String(self.expanseValue)
        
        arrData.append(dict)
        
        self.lblTotalIncome.text = "\(totalIncom)"
        self.lblTotalExpanse.text = "\(totalExpanse)"
        
        self.lblTotalProfit.text = "\(totalIncom-totalExpanse)"
//        print("self.lblTotalProfit: \(self.lblTotalProfit.text)")
        
//        print("arrData : \(arrData)")
        
    }
    
    /*
    func incomeFilter(_ index:Int)
    {
        incomeValue = 0
        var dict = JSON()
        for item in realm.objects(IncomeRealmDataBase.self).filter("monthIndex == \(index)") {
            self.incomeValue = self.incomeValue + Int(item.amount)!
//            var dict = JSON()
//            dict["income"].stringValue = String(self.incomeValue)
            //
            arrIncome.append(dict)
        }
        dict["index"].stringValue = String(index)
        dict["income"].stringValue = String(self.incomeValue)
        print("self.incomeValue:\(self.incomeValue)")
        print("arrExpanse : \(arrIncome)")
    }
    
    func expanseFilter(_ index:Int) {
        expanseValue = 0
        for item in realm.objects(ExpanceRealmDataBase.self).filter("monthIndex == \(index)") {
            self.expanseValue = self.expanseValue + Int(item.amount)!
            var dict = JSON()
            dict["expanse"].stringValue = String(self.expanseValue)
            dict["index"].stringValue = String(index)
            arrExpanse.append(dict)
        }
        print("arrExpanse : \(arrExpanse)")
    }
    */
    
    //MARK: Button Action
    @IBAction func btnShareAction(_ sender: Any) {
        
//        self.makePdfToTableData()
        viewDataMakePdf()
        pdfDataWithTableView(tableView: self.tblDataBase)
    }
    
    /*
     let priorBounds: CGRect = self.tblView.bounds
     let fittedSize: CGSize = self.tblView.sizeThatFits(CGSize(width: priorBounds.size.width, height: self.tblView.contentSize.height))
     self.tblView.bounds = CGRect(x: 0, y: 0, width: fittedSize.width, height: fittedSize.height)
     self.tblView.reloadData()
     
     let pdfPageBounds: CGRect = CGRect(x: 0, y: 0, width: fittedSize.width, height: (fittedSize.height))
     let pdfData: NSMutableData = NSMutableData()
     UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
     UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
     self.tblView.layer.render(in: UIGraphicsGetCurrentContext()!)
     UIGraphicsEndPDFContext()
     
     let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
     let documentsFileName = documentDirectories! + "/" + "pdfName"
     pdfData.write(toFile: documentsFileName, atomically: true)
     
     print(documentsFileName)
     */
    
    func makePdfToTableData() {
        
        
        //----For Table data
        let priorBounds : CGRect = self.tblDataBase.bounds
        let fittedSize : CGSize = self.tblDataBase.sizeThatFits(CGSize(width: priorBounds.size.width, height: self.tblDataBase.contentSize.height))
        self.tblDataBase.bounds = CGRect(x: 0, y: 0, width: fittedSize.width, height: 1600)
//        self.tblDataBase.bounds = priorBounds
        self.tblDataBase.reloadData()
        let pdfPageBounds : CGRect = CGRect(x: 0, y: 0, width: fittedSize.width, height: fittedSize.height)
        //----For make pdf
        let pdfData : NSMutableData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        self.tblDataBase.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsEndPDFContext()
        
        let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let documentsFileName = documentDirectories! + "/" + "Database"
        pdfData.write(toFile: documentsFileName, atomically: true)
        print("documentsFileName:---\(documentsFileName)")
        
    }
    
    func pdfDataWithTableView(tableView: UITableView) {
        
        //--- Table data pdf make
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:1385)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:self.view.frame.height)
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        //--- page increment pdf
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("myDocument.pdf")
        print("docTableURL: ---- \(docURL)")
        pdfData.write(to: docURL as URL, atomically: true)
        
    }
    
    func viewDataMakePdf() {
        let priorBounds = self.viewDataBaseBottom.bounds
        let filterSize = self.viewDataBaseBottom.sizeThatFits(CGSize(width: priorBounds.size.width, height: priorBounds.height))
        self.viewDataBaseBottom.bounds = CGRect(x: 0, y: 0, width: priorBounds.width, height: priorBounds.height)
        let pdfPageBounds = CGRect(x: 0, y: 0, width: priorBounds.width, height: priorBounds.height)
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
        var pageOriginY : CGFloat = 0
        
        while pageOriginY < filterSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()?.saveGState()
            UIGraphicsGetCurrentContext()?.translateBy(x: 0, y: -pageOriginY)
            viewDataBaseBottom.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()?.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        viewDataBaseBottom.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("myViewDocument.pdf")
        print("docViewURL:--- \(docURL)")
        pdfData.write(to: docURL as URL, atomically: true)
        
    }
    
}

//MARK: TableView DataSource, Delegate
extension DatabaseViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DatabaseTableViewCell", for: indexPath) as! DatabaseTableViewCell
        
        let dict = arrData[indexPath.row]
        
        cell.IncomelabelAmount.text = dict["income"].stringValue
        cell.ExpanselabelAmount.text = dict["expense"].stringValue
        cell.lblSection.text = GetMonthName(index: indexPath.row)

//        cell.IncomelabelAmount.text = dict["income"].stringValue
        
//        if dict["index"].stringValue == "\(0)" {
//            cell.lblSection.text = "January"
//        } else if dict["index"].stringValue == "\(1)" {
//            cell.lblSection.text = "Fabruary"
//        } else if dict["index"].stringValue == "\(2)" {
//            cell.lblSection.text = "March"
//        } else if dict["index"].stringValue == "\(3)" {
//            cell.lblSection.text = "April"
//        } else if dict["index"].stringValue == "\(4)" {
//            cell.lblSection.text = "May"
//        } else if dict["index"].stringValue == "\(5)" {
//            cell.lblSection.text = "June"
//        } else if dict["index"].stringValue == "\(6)" {
//            cell.lblSection.text = "July"
//        } else if dict["index"].stringValue == "\(7)" {
//            cell.lblSection.text = "Auguest"
//        } else if dict["index"].stringValue == "\(8)" {
//            cell.lblSection.text = "September"
//        } else if dict["index"].stringValue == "\(9)" {
//            cell.lblSection.text = "Octomber"
//        } else if dict["index"].stringValue == "\(10)" {
//            cell.lblSection.text = "November"
//        } else if dict["index"].stringValue == "\(11)" {
//            cell.lblSection.text = "December"
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK:- Other methods
extension DatabaseViewController
{
    func GetMonthName(index : Int) -> String
    {
        return monthArray[index]
    }
}
