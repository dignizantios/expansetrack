//
//  TopBarViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit
import CarbonKit

class TopBarViewController: UIViewController, CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var mainView: UIView!
    
    var items = [String]()
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var navigationTitle = String()

    var monthIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        items = ["Income", "Expense"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
//        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
//        self.mainView.addSubview(carbonTabSwipeNavigation.view)
        setupUI()
    }
    
    func setupUI() {
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
//        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor(displayP3Red: 38/255, green: 28/255, blue: 171/255, alpha: 1.0)
//        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.clear
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.size.width / 2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.size.width / 2, forSegmentAt: 1)
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.setIndicatorHeight(2)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor(displayP3Red: 38/255, green: 28/255, blue: 171/255, alpha: 1.0))
        carbonTabSwipeNavigation.setNormalColor(UIColor.darkGray)
        carbonTabSwipeNavigation.setSelectedColor(UIColor(displayP3Red: 38/255, green: 28/255, blue: 171/255, alpha: 1.0))
        
//        carbonTabSwipeNavigation.setNormalColor(UIColor.black, font: UIFont.init(name: "SanFranciscoDisplay-Regular", size: 15)!)
//        carbonTabSwipeNavigation.setSelectedColor(UIColor.red, font: UIFont.init(name: "SanFranciscoDisplay-Bold", size: 15)!)
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = self.navigationTitle
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navigationLeftButton())
    }
    
    @IBAction func buttonDatabaseAction(_ sender: Any) {
//        let databaseVC = self.storyboard?.instantiateViewController(withIdentifier: "DatabaseViewController") as! DatabaseViewController
//        
//        self.navigationController?.pushViewController(databaseVC, animated: true)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        print("abcde")
        
//        var screen = UIViewController()
        if index == 0 {
            print("-----------------------------")
           let indexScreen = storyboard?.instantiateViewController(withIdentifier: "IncomeViewController") as! IncomeViewController
            
            indexScreen.selectMonthIndex = self.monthIndex
            
            return indexScreen
        }
        else if index == 1 {
            print("+++++++++++++++++++++++++++++")
            let expanseScreen = self.storyboard?.instantiateViewController(withIdentifier: "ExpenseViewController") as! ExpenseViewController
            
            expanseScreen.selectMonthIndex = self.monthIndex
            
            return expanseScreen
        }
        else {
            let screen = storyboard?.instantiateViewController(withIdentifier: "TopBarViewController") as! TopBarViewController
            
            return screen
        }
        
    }

}
