//
//  ExpenseViewController.swift
//  ExpanseTrack
//
//  Created by Haresh on 05/04/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class ExpenseViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var textFieldName    : UITextField!
    @IBOutlet weak var textFieldAmount  : UITextField!
    @IBOutlet weak var buttonAdd        : UIButton!
    @IBOutlet weak var tblExpanseData   : UITableView!
    @IBOutlet weak var viewExpance: UIView!
    @IBOutlet weak var btnDone: UIButton!
    
    var selectMonthIndex = Int()
    var monthValueArray = NSMutableArray()
    
    var expensedataResult : Results<ExpanceRealmDataBase>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Month Index:", selectMonthIndex)
        expensedataResult = realm.objects(ExpanceRealmDataBase.self)
        
        self.tblExpanseData.tableFooterView = UIView()
        self.viewExpance.isHidden = true
        self.tblExpanseData.register(UINib(nibName: "ExpanseTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpanseTableViewCell")
        print("Expance Realm File Path URL:", Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        [textFieldName, textFieldAmount].forEach { (textField) in
            textField?.setLeftPaddingPoints(10)
        }
        self.tblExpanseData.isHidden = false
        self.viewExpance.isHidden = true
    }
    
    @IBAction func buttonAddAction(_ sender: Any) {
        self.tblExpanseData.isHidden = true
        self.viewExpance.isHidden = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldAmount {
            let allowedCharacters = "1234567890"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        return true
    }
    
    func addData() {
        let expanceData = ExpanceRealmDataBase()
        let pId = expanceData.id
        expanceData.id = incrementID()
        
        expanceData.monthIndex = self.selectMonthIndex
        expanceData.expanceName = self.textFieldName.text!
        expanceData.amount = self.textFieldAmount.text!
        
        try! realm.write {
            realm.add(expanceData)
        }
        
    }
    
    //Primary key increment
    func incrementID() -> Int {
        return (realm.objects(ExpanceRealmDataBase.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }

    @IBAction func btnDoneAction(_ sender: Any) {
        if (self.textFieldName.text?.isEmpty == true) || (self.textFieldAmount.text?.isEmpty == true) {
            self.displayAlertMessage(userMessage: "Please Fill all textField")
        }
        else {
            textFieldAmount.resignFirstResponder()
            self.addData()
            textFieldAmount.text    = ""
            textFieldName.text      = ""
            self.viewExpance.isHidden = true
            self.tblExpanseData.isHidden = false
            self.displayAlertMessage(userMessage: "New Data add")
        }
        self.tblExpanseData.reloadData()
        self.tblExpanseData.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.monthValueArray.removeAllObjects()
        for item in realm.objects(ExpanceRealmDataBase.self).filter("monthIndex == \(self.selectMonthIndex)") {
            self.monthValueArray.add(item)
        }
        return self.monthValueArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpanseTableViewCell", for: indexPath) as! ExpanseTableViewCell
        
        let expanseDic = self.monthValueArray[indexPath.row]
        
        cell.lblDataName.text = (expanseDic as AnyObject).expanceName
        cell.lblDataAmount.text = (expanseDic as AnyObject).amount
        
        return cell
    }
}
